﻿using System;
using System.Collections.Generic;

namespace InsCalc
{
    public struct InsData
    {
        public int Age;
        public int SumAssured;
        public double GrossPremium;
    }
    public class InsCalculator
    {
        private Dictionary<int, double[]> riskBandCol;
        private Dictionary<int, double[]> RiskBandCol
        {
            get
            {
                if (riskBandCol == null)
                {
                    riskBandCol = new Dictionary<int, double[]>();
                    double[] valFor25000 = { 0.0172, 0.1043, 0.2677 };
                    double[] valFor50000 = { 0.0165, 0.0999, 0.2565 };
                    double[] valFor100000 = { 0.0154, 0.0932, 0.2393 };
                    double[] valFor200000 = { 0.0147, 0.0887, 0.2285 };
                    double[] valFor300000 = { 0.0144, 0.0872 };
                    double[] valFor500000 = { 0.0146 };

                    riskBandCol.Add(25000, valFor25000);
                    riskBandCol.Add(50000, valFor50000);
                    riskBandCol.Add(100000, valFor100000);
                    riskBandCol.Add(200000, valFor200000);
                    riskBandCol.Add(300000, valFor300000);
                    riskBandCol.Add(500000, valFor500000);
                }
                return riskBandCol;
            }
        }

        public InsData GetLifeInsPre(int age, int sumAssured)
        {
            InsData insData = new InsData() { Age = age , SumAssured = sumAssured};
            if ((age < 18 || age > 65) || (sumAssured < 25000 || sumAssured > 500000))
            {
                insData.GrossPremium = 0;
                return insData;
            }
            if ((age > 50 && sumAssured > 200000) || (age > 30 && sumAssured > 300000))
            {
                insData.GrossPremium = 0;
                return insData;
            }
            return CalLifeInsPre(ref insData);
        }

        private InsData CalLifeInsPre(ref InsData insData)
        {
            double riskRate = GetRiskRate(insData.Age, insData.SumAssured);
            if (riskRate == 0)
            {
                insData.GrossPremium = 0;
                return insData;
            }
            double riskPremium = riskRate * (insData.SumAssured / 1000);
            var renewalCommission = 0.03 * riskPremium;
            var netPremium = riskPremium + renewalCommission;
            var initialCommission = netPremium * 2.05;
            insData.GrossPremium = netPremium + initialCommission;

            if (insData.GrossPremium < 2)
            {
                insData.SumAssured += 5000;
                CalLifeInsPre(ref insData);
            }

            return insData;
        }

        private double GetRiskRate(int age, int sumAssured )
        {
            double riskRate = 0;
            double lowerRiskRate = 0.0, upperRiskRate = 0.0;

            int lowerSumAsrd =0, upperSumAsrd =0;
            
            int column = 0;
            if (age > 30 && age <= 50)
                column = 1;
            if (age > 50)
                column = 2;
            int counter = 0;
            int[] keys = new int[RiskBandCol.Keys.Count];
            RiskBandCol.Keys.CopyTo(keys, 0);
            try
            {
                foreach (int sum in RiskBandCol.Keys)
                {
                    if (sum == sumAssured)
                    {
                        riskRate = RiskBandCol[sum][column];
                        break;
                    }
                    if (keys[counter] < sumAssured && sumAssured < keys[counter + 1])
                    {
                        lowerSumAsrd = keys[counter];
                        upperSumAsrd = keys[counter + 1];
                        lowerRiskRate = RiskBandCol[keys[counter]][column];
                        upperRiskRate = RiskBandCol[keys[counter + 1]][column];
                        break;
                    }
                    counter++;
                }
            }
            catch(Exception iex)
            {
                return riskRate;
            }
          
            if (riskRate == 0)
                riskRate = ((double)(sumAssured - lowerSumAsrd) / (double)(upperSumAsrd - lowerSumAsrd) * upperRiskRate + (double)(upperSumAsrd - sumAssured) / (double)(upperSumAsrd - lowerSumAsrd) * lowerRiskRate);

            return riskRate;
        }

        
    }
}
