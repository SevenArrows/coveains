﻿using InsCalc;
using System;

namespace CoveaIns
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            InsCalculator inscal = new InsCalculator();
            InsData insData = inscal.GetLifeInsPre(18, 25000);
            Console.WriteLine( "Age : "+ insData.Age );
            Console.WriteLine("Sum Assured : " + insData.SumAssured);
            Console.WriteLine("Gross Premium : " + (insData.GrossPremium != 0 ? insData.GrossPremium.ToString() : "No premium avialable for this age or sum assured "));
        }
    }
}
