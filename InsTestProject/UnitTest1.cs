using InsCalc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsTestProject
{
    [TestClass]
    public class UnitTest1
    {
        private InsCalculator insCalculator;
        public UnitTest1()
        {
            insCalculator = new InsCalculator();
        }
        [DataTestMethod]
        [DataRow(12, 1000 , 0)]
        [DataRow(35, 580000,0)]
        [DataRow(67, 4679, 0)]
        public void CheckAgeSumAsrdRange(int age , int sumAssured,double grossPremium)
        {
            InsData insData = insCalculator.GetLifeInsPre(age, sumAssured);
            Assert.AreEqual(grossPremium, insData.GrossPremium);
        }

        [DataTestMethod]
        [DataRow(18, 25000,2)]
        [DataRow(30, 50000,2)]
        [DataRow(49, 60000, 2)]
        public void TestRiskRate(int age, int sumAssured, double grossPremium)
        {
            InsData insData = insCalculator.GetLifeInsPre(age, sumAssured);
            Assert.IsTrue(grossPremium <= insData.GrossPremium);
        }
    }
}
